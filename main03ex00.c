#include <stdio.h>
#include <string.h>
int ft_strcmp(char *s1, char *s2);

int	main(void)
{
	char *s1 = "test";
	char *s2 = "testing";
	char *s3 = "";

	printf("real: %i | ft: %i\n", strcmp(s1, s2), ft_strcmp(s1, s2));
	printf("real: %i | ft: %i\n", strcmp(s1, s3), ft_strcmp(s1, s2));
}
